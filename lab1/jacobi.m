% jacobi.m

function [out, abs_max] = jacobi(A, B)
    n=4;
    % pomocnicze macierze
    c(1:n,1:n) = 0;
    d(1:n,1:n) = 0;
    for i=1:n
        for j=1:n
            if i==j
                d(i,j)=A(i,j);
            else
                c(i,j)=A(i,j);
            end
        end
    end
    d=inv(d);
    x(1:n,1:1)=10000000;
    y(1:n,1:1)=0;
    step=0;
    % właściwe iteracje
    while(max(abs(x-y)) > 0.0000001);
        x=y;
        y=d*(B-c*x);
        step=step+1;
        out{step} = y;
        abs_max(step) = max(abs(x-y));
    end
end