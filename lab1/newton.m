%newton.m
%newton
% f=input(’podaj wzor f(x)=’,’s’);
% df=input(’podaj wzor na df/dx=’,’s’);
% x=input(’podaj x0=’);
% eps=input(’podaj eps=’);
function [out, errors] = newton(f, df, x, eps)
    del=1000000;
    iter = 1;
    while del > eps
        x1=x-eval(f)/eval(df);
        del=abs(x1-x)
        x=x1;
        errors(iter) = del;
        out(iter) = x;
        iter += 1;
    end
    format long
disp('newton ends');
end
x=x