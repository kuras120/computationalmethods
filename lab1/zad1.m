N = [10 20 50 90];

a = -pi/4;
b = pi/4;
f = 'cos(x)+asin(x)';

toCompare = intpara(10000, a, b, f);
for i = 1:length(N)
    resultsTria(i) = inttria(N(i), a, b, f);
    resultsPara(i) = intpara(N(i), a, b, f);
    errorsTria(i) = abs(toCompare - resultsTria(i));
    errorsPara(i) = abs(toCompare - resultsPara(i));
end
plt = figure;
plot(log10(N), log10(errorsTria), log10(N), log10(errorsPara));
title('Badanie błędu funkcji całkującej');
xlabel('log10(n)');
ylabel('log10(error)');
legend('inttria', 'intpara');
saveas(plt, 'zad1.png');

tanAngleTria = polyfit(log10(N), log10(errorsTria), 1);
tanAnglePara = polyfit(log10(N), log10(errorsPara), 1);

disp(['Dane tria: ', mat2str(resultsTria)]);
disp(['Dane para: ', mat2str(resultsPara)]);
disp(['Error tria: ', mat2str(errorsTria)]);
disp(['Error para: ', mat2str(errorsPara)]);
disp(['Tangens kierunkowy tria: ', num2str(tanAngleTria(1))]);
disp(['Tangens kierunkowy para: ', num2str(tanAnglePara(1))]);