syms x;
f = cos(x)+asin(x);
df = diff(f);
a = -pi/4;
b = pi/4;
[output_newton, errors_newton] = newton(f, df, a, 1e-10);
[output_bisect, errors_bisect] = bisect(f, a, b, 1e-10);
[output_siecz, errors_siecz] = siecz(f, a, b, 1e-10);

plt = figure;
plot(1:1:length(output_newton), log10(errors_newton), 
    1:1:length(output_bisect), log10(errors_bisect),
    1:1:length(output_siecz), log10(errors_siecz));
title('Badanie błędu funkcji rozwiązywania równania nieliniowego');
xlabel('n');
ylabel('log10(error)')
legend('newton', 'bisect', 'siecz');
saveas(plt, 'zad2.png');

disp(['Dane newton: ', mat2str(output_newton)]);
disp(['Dane bisect: ', mat2str(output_bisect)]);
disp(['Dane siecz: ', mat2str(output_siecz)]);
disp(['Error newton: ', mat2str(errors_newton)]);
disp(['Error bisect: ', mat2str(errors_bisect)]);
disp(['Error siecz: ', mat2str(errors_siecz)]);
