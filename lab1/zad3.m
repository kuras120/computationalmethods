a = -8;
b = -16;
c = -11;
d = 12;

my_matrix = [-8 2 4 1; 5 -16 7 3; 2 3 -11 4; 9 1 1 12];
[V, D] = eig(my_matrix);
disp('Z def. matlab: A * V = V * D => A = V * D * (V^-1)');
disp('V*D*(V^-1)')
disp(round(V*D*(V^-1)));
disp('A')
disp(my_matrix);
disp('Macierze są równe (zastosowane zaokrąglenie w związku z ograniczoną dokładnością)')

disp('Sprawdzenie prostopadłości: ')
vecs = {V(:,1), V(:,2), V(:,3), V(:,4)};

ifZeroThenTrue = 0;
for i = 1:length(vecs)
    for j = i+1:length(vecs)
        ifZeroThenTrue += dot(cell2mat(vecs(1, i)), cell2mat(vecs(1, j)));
    end
end
if disp(ifZeroThenTrue) == 0
    disp('Wektory są prostopadłe');
else
    disp('Wektory NIE są prostopadłe');
end

disp('Porównanie sumy diagonalnej macierzy A z sumą wartości własnych: ')
if sum(diag(round(my_matrix))) == sum(diag(round(D)))
    disp('Sumy są równe');
else
    disp('Sumy NIE są równe');
end

disp('Porównanie wyznacznika macierzy A z iloczynem wartości własnych: ');
if round(det(my_matrix)) == round(prod(diag(D)))
    disp('Wartości są równe');
else
    disp('Wartości NIE są równe');
end