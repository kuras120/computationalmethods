my_matrix = [-8 2 4 1; 5 -16 7 3; 2 3 -11 4; 9 1 1 12];
b_matrix = [1;2;3;4];
[out, abs_max] = jacobi(my_matrix, b_matrix);
X = linsolve(my_matrix, b_matrix);
for i = 1:length(abs_max)
    errors(i) = abs(sum(out{1,i}-X));
end
plt = figure;
plot(1:1:length(abs_max), log10(errors));
title('Badanie metody Jacobiego');
xlabel('Numer iteracji');
ylabel('log10(error)');

saveas(plt, 'zad4.png');

tanAngleJacob = polyfit(log10(1:1:length(abs_max)), log10(errors), 1);

disp('Wyniki: ');
disp(out);
disp('Błędy: ');
disp(errors);
disp('Stopnie zbieżności: ');
disp(tanAngleJacob(1));